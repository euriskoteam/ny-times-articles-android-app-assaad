package com.apps2you.nytimeapplication;

import android.content.Context;
import android.os.Handler;
import android.support.multidex.MultiDexApplication;

import com.facebook.drawee.backends.pipeline.Fresco;


public class ApplicationContext extends MultiDexApplication {

    private static ApplicationContext instance;
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    public static ApplicationContext getContext() {

        if (instance == null)
            instance = new ApplicationContext();

        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Fresco.initialize(this);

    }

    public ApplicationContext() {
        instance = this;
    }

    private static volatile Handler applicationHandler = null;

    public static Handler getAppHandler() {
        if (applicationHandler == null)
            applicationHandler = new Handler(getContext().getMainLooper());
        return applicationHandler;
    }



}
