package com.apps2you.nytimeapplication.connectivity;

import java.net.URLConnection;

public class FileToUpload {
        String keyName = "file";
        String path;
        private String mime;

        public  FileToUpload(String key, String path) {
                this.keyName = key;
                this.path = path;
                this.setMime(this.findMimeFromExtensiont(this.getFileName().split("\\.")[1]));
        }
        private String findMimeFromExtensiont(String fileExtension) {
                return URLConnection.guessContentTypeFromName(fileExtension);
        }

        public String getFileName(){
                return path.substring(path.lastIndexOf("/")+1);
        }


        public String getKeyName(){
                return keyName;
        }

        public String getMime(){
                return mime;
        }


        public String setMime(String mime){
                return this.mime = mime;
        }

        public String getPath(){
                return path;
        }

}