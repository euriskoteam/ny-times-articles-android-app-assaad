package com.apps2you.nytimeapplication.connectivity;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.os.ResultReceiver;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcels;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * helper methods.
 */
public class APIService extends IntentService {


    public static final int STATUS_STARTED = 0;
    public static final int STATUS_SUCCESS = 1;
    public static final int STATUS_CONNECTION_ERROR = 2;
    public static final int STATUS_SERVER_ERROR = 3;

    String response;

    public static final String HEADERS = "HEADERS";
    public static final String URL = "URL";
    public static final String ACTION = "action";
    public static final String DATA_SENT = "data_sent";
    public static final String DATA_SERVER_ERROR_CODE = "data_SERVER_ERROR_CODE";
    public static final String DATA_SERVER_MESSAGE = "data_SERVER_MESSAGE";

    public static final int GET_TYPE = 0;
    public static final int POST_TYPE = 1;
    public static final int PUT_TYPE = 2;
    public static final int POST_FORM_DATA_TYPE = 3;
    public static final int POST_MULTIPART_TYPE = 4;
    public static final int PUT_MULTIPART_TYPE = 5;

    public static final String HIT_TYPE = "HIT_TYPE";

    public static final String linkToUpload = "linkToUpload";
    public static final String linkToUploadKey = "linkToUploadKey";


    public APIService() {
        super("APIService");
    }


    @Override
    public void onCreate() {
        super.onCreate();

    }

    public void cancel() {

    }


    public static FileToUpload[] generateFileToUploadObjects(ArrayList<String> filePath, ArrayList<String> key) {

        FileToUpload[] fileToUploads = new FileToUpload[filePath.size()];

        for (int i = 0; i < filePath.size(); i++) {
            fileToUploads[i] = new FileToUpload(key.get(i), filePath.get(i));
        }

        return fileToUploads;


    }


    @SuppressLint("RestrictedApi")
    @Override
    protected void onHandleIntent(final Intent intent) {

        if (intent != null) {
            final Bundle bundle = new Bundle();
            final ResultReceiver receiver = intent.getParcelableExtra("receiver");

            try {
                /* Update UI: Download Service is Running */
                bundle.putString(ACTION, intent.getAction());
                bundle.putParcelable(DATA_SENT, intent.getExtras().getParcelable(DATA_SENT));

                if (receiver != null)
                    receiver.send(STATUS_STARTED, bundle);


                response = makeHit(intent.getStringExtra(URL), intent.getSerializableExtra(HEADERS),
                        intent.getExtras().getParcelable(DATA_SENT),
                        intent.getExtras().getInt(HIT_TYPE),
                        generateFileToUploadObjects(intent.getStringArrayListExtra(linkToUpload), intent.getStringArrayListExtra(linkToUploadKey))
                );


                String jsonString = new Gson().toJson(Parcels.unwrap(intent.getExtras().getParcelable(DATA_SENT)).toString());
                Log.d("testRequest", jsonString);

                response = removeDoubleQuotes(response);
                bundle.putString(Intent.EXTRA_TEXT, response);
                BaseResponse response = new Gson().fromJson(bundle.getString(Intent.EXTRA_TEXT), new TypeToken<BaseResponse>() {
                }.getType());

                if (isSuccess( response, receiver, bundle)) {
                    receiver.send(STATUS_SUCCESS, bundle);
                }

            } catch (Exception e) {
                bundle.putString(Intent.EXTRA_TEXT, e.toString());
                BaseResponse response = new BaseResponse();
                response.setMessage(e.toString());
                response.setStatus("failed");
                if (receiver != null) {
                    receiver.send(STATUS_CONNECTION_ERROR, bundle);
                }
            }
        }
    }



    private String postData(Parcelable object) {



//        HashMap<String,Object> hashMap = getHashMapFromObject(object);
//       if( hashMap.containsKey("contents"))
//           return   removeDoubleQuotes(hashMap.get("contents").toString());
//       else {
           String stringJson = null;
           String post = new Gson().toJson(Parcels.unwrap(object));
           stringJson = removeDoubleQuotes(post).replaceAll("\\\\", "");
           return stringJson;
//       }


    }

    @SuppressLint("RestrictedApi")
    protected boolean isSuccess(BaseResponse baseResponse, ResultReceiver receiver, Bundle bundle) {

        switch (baseResponse.getStatus()) {
            case "OK":
                return true;

        }
        bundle.putString(DATA_SERVER_MESSAGE,baseResponse.getMessage());
        bundle.putString(DATA_SERVER_ERROR_CODE, baseResponse.getStatus());
        receiver.send(STATUS_SERVER_ERROR, bundle);
        return false;
    }

    public HashMap<String, Object> getHashMapFromObject(Object object) {
        HashMap<String, Object> hashMap = new HashMap<>();
        String stringJson = null;
        final Gson gson = new Gson();
        String post = gson.toJson(object).toString();
        stringJson = removeDoubleQuotes(post);

        JSONObject jsonObj = null;
        try {
            jsonObj = new JSONObject(stringJson);
            hashMap = (HashMap<String, Object>) jsonToMap(jsonObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return hashMap;
    }


    public static HashMap<String, Object> getHashMapFromString(String object) {
        HashMap<String, Object> hashMap = new HashMap<>();
        JSONObject jsonObj = null;
        try {
            jsonObj = new JSONObject(object);
            hashMap = (HashMap<String, Object>) jsonToMap(jsonObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return hashMap;
    }


    public static String removeDoubleQuotes(String input_text) {
        String output_text = input_text;
        if (output_text.startsWith("\"")) {
            output_text = output_text.substring(1);
        }
        if (output_text.endsWith("\"")) {
            output_text = output_text.substring(0, output_text.length() - 1);
        }
        if (output_text.startsWith("\"") || output_text.endsWith("\"")) {
            removeDoubleQuotes(output_text);
        }
        return output_text;
    }

    public static Map<String, Object> jsonToMap(JSONObject json) throws JSONException {
        Map<String, Object> retMap = new HashMap<String, Object>();

        if (json != JSONObject.NULL) {
            retMap = toMap(json);
        }
        return retMap;
    }

    public static Map<String, Object> toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keys();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    public static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }


    public ApiInterface getApiInterface() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder clientBuilder = (new OkHttpClient.Builder()).addInterceptor(interceptor);
        clientBuilder.readTimeout(60L, TimeUnit.SECONDS);
        clientBuilder.connectTimeout(60L, TimeUnit.SECONDS);
        OkHttpClient client = clientBuilder.build();
        Retrofit retrofit = (new Retrofit.Builder()).baseUrl("http://www.google.com").addConverterFactory(ScalarsConverterFactory.create()).client(client).build();
        return retrofit.create(ApiInterface.class);
    }

    public String makeHit(String url, Serializable headBundle, Parcelable bodyBundle, int type, FileToUpload... files) throws IOException {
        Call<String> call = null;

        switch (type) {
            case GET_TYPE:
                call = getApiInterface().getHTTP(url, (Map) headBundle);
                return call.execute().body();
            case POST_TYPE:
                call = getApiInterface().postHTTP(url, postData(bodyBundle), (Map) headBundle);
                return call.execute().body();
            case PUT_TYPE:
                call = getApiInterface().putHTTP(url, postData(bodyBundle), (Map) headBundle);
                return call.execute().body();
            case POST_FORM_DATA_TYPE:
                HashMap<String, Object> hashMap = getHashMapFromObject(Parcels.unwrap(bodyBundle));
                call = getApiInterface().postFormData(url, (Map) hashMap, (Map) headBundle);
                return call.execute().body();
            case POST_MULTIPART_TYPE:
                return sendHttpRequest(url, headBundle, bodyBundle, POST_TYPE, files);
            case PUT_MULTIPART_TYPE:
                return sendHttpRequest(url, headBundle, bodyBundle, PUT_TYPE, files);
            default:
                return "";
        }


    }

    public String sendHttpRequest(String url, Serializable headBundle, Parcelable bodyBundle, int type, FileToUpload... files) throws IOException {
        OkHttpClient client = new OkHttpClient();
        MultipartBody.Builder requestBuilder = (new MultipartBody.Builder()).setType(MultipartBody.FORM);
        Iterator var8 = getHashMapFromObject(Parcels.unwrap(bodyBundle)).entrySet().iterator();
        while (var8.hasNext()) {
            Map.Entry<String, Object> part = (Map.Entry) var8.next();
            if (part.getValue() instanceof List) {
                requestBuilder.addFormDataPart(part.getKey(), new Gson().toJson(part.getValue()));
            } else {
                requestBuilder.addFormDataPart(part.getKey(), String.valueOf(part.getValue()));
            }
        }

        Headers headers = null;
        if (headBundle != null) {
            Map<String, String> headerMap = (Map) headBundle;
            Headers.Builder headerBuilder = new Headers.Builder();
            Iterator var11 = headerMap.keySet().iterator();

            while (var11.hasNext()) {
                String key = (String) var11.next();
                headerBuilder.add(key, (String) headerMap.get(key));
            }

            headers = headerBuilder.build();
        }

        FileToUpload[] var15 = files;
        int var17 = files.length;

        for (int var19 = 0; var19 < var17; ++var19) {
            FileToUpload file = var15[var19];
            requestBuilder.addFormDataPart(file.getKeyName(), file.getFileName(), RequestBody.create(MediaType.parse(file.getMime()), getBytes(file.getPath())));
        }

        MultipartBody req = requestBuilder.build();
        Request.Builder reqBuilder = (new Request.Builder()).url(url);
        switch (type) {
            case POST_TYPE:
                reqBuilder.post(req);
                break;
            case PUT_TYPE:
                reqBuilder.put(req);
                break;
        }

        if (headers != null) {
            reqBuilder.headers(headers);
        }

        Request request = reqBuilder.build();
        okhttp3.Call call = client.newCall(request);
        return call.execute().body().string();
    }


    private byte[] getBytes(String path) {
        File file = new File(path);
        int size = (int) file.length();
        byte[] bytes = new byte[size];

        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
            buf.read(bytes, 0, bytes.length);
            buf.close();
            return bytes;
        } catch (FileNotFoundException var6) {
            var6.printStackTrace();
            return null;
        } catch (IOException var7) {
            var7.printStackTrace();
            return null;
        }
    }
}



