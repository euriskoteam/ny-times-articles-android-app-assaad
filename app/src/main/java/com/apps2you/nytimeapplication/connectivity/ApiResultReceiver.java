package com.apps2you.nytimeapplication.connectivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.os.ResultReceiver;

import org.parceler.Parcels;

/**
 * Created by koa on 4/18/2016.
 */
@SuppressLint("ParcelCreator")
public class ApiResultReceiver extends ResultReceiver {

    private Receiver mReceiver;

    @SuppressLint("RestrictedApi")
    public ApiResultReceiver(Handler handler) {
        super(handler);
    }

    public void setReceiver(Receiver receiver) {
        mReceiver = receiver;
    }

    public interface Receiver {
        void onHttpResponse(String action, String objectReceived, Object objectSent);

        void onHttpRequestStarted(String action, Object objectSent);

        void onConnectionError(String action, Object objectSent);

        void onResponseServerError(String action, String errorCode, String objectReceived, Object objectSent);

    }


    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if (mReceiver != null) {
            switch (resultCode) {
                case APIService.STATUS_STARTED:
                    mReceiver.onHttpRequestStarted(resultData.getString(APIService.ACTION),Parcels.unwrap(resultData.getParcelable(APIService.DATA_SENT)));
                    break;
                case APIService.STATUS_SUCCESS:
                    mReceiver.onHttpResponse(resultData.getString(APIService.ACTION),resultData.getString(Intent.EXTRA_TEXT),Parcels.unwrap(resultData.getParcelable(APIService.DATA_SENT)));
                    break;
                case APIService.STATUS_CONNECTION_ERROR:
                    mReceiver.onConnectionError(resultData.getString(APIService.ACTION),Parcels.unwrap(resultData.getParcelable(APIService.DATA_SENT)));
                    break;
                case APIService.STATUS_SERVER_ERROR:
                    mReceiver.onResponseServerError(resultData.getString(APIService.ACTION), resultData.getString(APIService.DATA_SERVER_ERROR_CODE), resultData.getString(Intent.EXTRA_TEXT),Parcels.unwrap(resultData.getParcelable(APIService.DATA_SENT)));
                    break;
            }

        }
    }
}