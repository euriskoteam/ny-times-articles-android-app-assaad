package com.apps2you.nytimeapplication.gallery;


import java.io.Serializable;

public interface GalleryObjectInterface extends Serializable {

    public  int getMediaType();
    public String getLink();
    public String getThumb();
    public String getName();
}
