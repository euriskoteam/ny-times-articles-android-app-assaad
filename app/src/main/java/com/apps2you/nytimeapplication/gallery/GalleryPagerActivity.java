package com.apps2you.nytimeapplication.gallery;

/**
 * Created by appsassaad on 2/26/2018.
 */
import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Animatable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.PermissionRequest;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.apps2you.nytimeapplication.BaseActivity;
import com.apps2you.nytimeapplication.R;
import com.apps2you.nytimeapplication.connectivity.ApiResultReceiver;
import com.apps2you.nytimeapplication.gallery.zoomableDrawee.zoomable.ZoomableDraweeView;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.drawable.ScalingUtils;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.image.ImageInfo;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class GalleryPagerActivity extends AppCompatActivity implements VideoFragment.OnFragmentInteractionListener {

    public static void Launch(Activity activity, ArrayList<GalleryObjectInterface> galleryObjectInterfaces, int position) {
        Intent intent = new Intent(activity, GalleryPagerActivity.class);

        Bundle bundle = new Bundle();

        bundle.putSerializable(GalleryPagerActivity.DATA, galleryObjectInterfaces);
        intent.putExtra(GalleryPagerActivity.DATA, bundle);
        intent.putExtra(GalleryPagerActivity.EXTRA_POSITION, position);
        activity.startActivity(intent);
    }

    public static final int TYPE_TEXT = 0;
    public static final int TYPE_IMAGE = 1;
    public static final int TYPE_VIDEO = 2;
    public static final int TYPE_AUDIO = 3;
    public static final int TYPE_GIF = 7;

    public static final String EXTRA_POSITION = "position";
    public static final String ENABLE_SHARE = "ENABLE_SHARE";
    public static final String DATA = "DATA";

    boolean  enabelShare = false;

    private ViewPager mViewPager;

    int position = 0;

    ArrayList<GalleryObjectInterface> mediaCursor;

    ImageView ivBack;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_pager_layout);


        mViewPager = (HackyViewPager) findViewById(R.id.view_pager);

        ivBack=(ImageView)findViewById(R.id.ivBack);

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Bundle bundle = getIntent().getBundleExtra(DATA);
        mediaCursor = (ArrayList<GalleryObjectInterface>) bundle.getSerializable(DATA);
        enabelShare = bundle.getBoolean(ENABLE_SHARE, false);

        position = getIntent().getIntExtra(EXTRA_POSITION, 0);

        mViewPager.setAdapter(new SamplePagerAdapter());

        mViewPager.setCurrentItem(position);

            ((RelativeLayout) findViewById(R.id.share_layout)).setVisibility(View.VISIBLE);
            findViewById(R.id.share).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    share(mViewPager.getCurrentItem());
                }
            });

            findViewById(R.id.download).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkStoragePermission();

                }
            });

    }

    private void checkStoragePermission() {


        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {

                        if (report.areAllPermissionsGranted()) {

                            download(mViewPager.getCurrentItem());

                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<com.karumi.dexter.listener.PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }



    class SamplePagerAdapter extends PagerAdapter {


        @Override
        public int getCount() {
            return mediaCursor.size();
        }

        @Override
        public View instantiateItem(ViewGroup container, final int position) {

            if (mediaCursor.get(position).getMediaType() == TYPE_IMAGE) {

                View view = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_gallery_pager, null);
                loadImageInto(view, position);
                container.addView(view, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                return view;

            } else if (mediaCursor.get(position).getMediaType() == TYPE_VIDEO) {

                View view = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_play_video, null);

                ImageView imageView = (ImageView) view.findViewById(R.id.play);
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String url = mediaCursor.get(position).getLink();
                        if (!url.startsWith("http") && !url.startsWith("file"))
                            url = "file://" + url;
                        openMedia(url);
                    }
                });
                String thumb = mediaCursor.get(position).getThumb();
                container.addView(createDrawableFromView(view, thumb), ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                return view;

            } else if (mediaCursor.get(position).getMediaType() == TYPE_AUDIO) {

                final View view = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.audio_fragment, null);

                String url = mediaCursor.get(position).getLink();

                Uri mUri = Uri.parse(url);
                if (lastAudio != null) {
                    if (lastAudio.mMediaPlayer != null)
                        if (lastAudio.mMediaPlayer.isPlaying())
                            lastAudio.mMediaPlayer.pause();
                }
                lastAudio = new Audio(view, mUri);
                lastAudio.initialize();

                container.addView(view, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                return view;
            }
            return null;
        }


        void loadImageInto(final View view, final int position) {

            ZoomableDraweeView photo = (ZoomableDraweeView) view.findViewById(R.id.image);

            final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progresssBar);
            final Button retry = (Button) view.findViewById(R.id.retry_btn);

            String imageUrl = mediaCursor.get(position).getLink();

            Uri uri = Uri.parse(imageUrl);


            photo.setController(Fresco.newDraweeControllerBuilder()
                    .setUri(uri)
                    .setControllerListener(new ControllerListener<ImageInfo>() {
                        @Override
                        public void onSubmit(String id, Object callerContext) {

                        }

                        @Override
                        public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable animatable) {
                            progressBar.setVisibility(View.GONE);
                            //    attacher.update();
                        }

                        @Override
                        public void onIntermediateImageSet(String id, ImageInfo imageInfo) {

                        }

                        @Override
                        public void onIntermediateImageFailed(String id, Throwable throwable) {

                        }

                        @Override
                        public void onFailure(String id, Throwable throwable) {
                            progressBar.setVisibility(View.GONE);
                            retry.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onRelease(String id) {

                        }

                    }).build());
//            }

            GenericDraweeHierarchy hierarchy = new GenericDraweeHierarchyBuilder(getResources())
                    .setActualImageScaleType(ScalingUtils.ScaleType.FIT_CENTER)
                    .setProgressBarImage(new CircleProgressBarDrawable())
                    .build();
            photo.setHierarchy(hierarchy);

        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
    }

    Audio lastAudio;

    public class Audio {
        public Audio(View view, Uri mUri) {
            this.view = view;
            mPlayButton = view.findViewById(R.id.play);
            mSeekBar = view.findViewById(R.id.media_seekbar);
            mRunTime = view.findViewById(R.id.run_time);
            mPauseButton = view.findViewById(R.id.pause);
            mTotalTime = view.findViewById(R.id.total_time);
            this.mUri = mUri;
        }

        View view;
        TextView mRunTime;
        SeekBar mSeekBar;
        MediaPlayer mMediaPlayer;
        ImageView mPlayButton;
        TextView mTotalTime;
        ImageView mPauseButton;

        Handler mProgressUpdateHandler;
        Uri mUri;
        private static final int AUDIO_PROGRESS_UPDATE_TIME = 100;

        public void pause() {

            if (mMediaPlayer == null) {
                return;
            }

            if (mMediaPlayer.isPlaying()) {
                mMediaPlayer.pause();
                setPlayable();
            }
        }


        public void initialize() {
            initPlayer(view.getContext());

            setTotalTime();
            updateRuntime(0);
            initMediaSeekBar();

            mPlayButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    play();
                }
            });

        }

        public void play() {


            mPauseButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pause();
                }
            });

            // if play button itself is null, the whole purpose of AudioWife is
            // defeated.
            mProgressUpdateHandler = new Handler();
            if (mPlayButton == null) {
                throw new IllegalStateException("ERROR_PLAYVIEW_NULL");
            }

            if (mUri == null) {
                throw new IllegalStateException("Uri cannot be null. Call init() before calling this method");
            }

            if (mMediaPlayer == null) {
                return;

            }

            mProgressUpdateHandler.postDelayed(mUpdateProgress, AUDIO_PROGRESS_UPDATE_TIME);


            // enable visibility of all UI controls.
            setViewsVisibility();


            if (mMediaPlayer.isPlaying()) {
                return;
            }

            mMediaPlayer.start();

            setPausable();
        }

        private void setPausable() {
            if (mPlayButton != null) {
                mPlayButton.setVisibility(View.GONE);
            }

            if (mPauseButton != null) {
                mPauseButton.setVisibility(View.VISIBLE);
            }
        }

        private void initMediaSeekBar() {

            if (mSeekBar == null) {
                return;
            }

            // update seekbar
            long finalTime = mMediaPlayer.getDuration();
            mSeekBar.setMax((int) finalTime);

            mSeekBar.setProgress(0);

            mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    mMediaPlayer.seekTo(seekBar.getProgress());

                    // if the audio is paused and seekbar is moved,
                    // update the play time in the UI.
                    updateRuntime(seekBar.getProgress());
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                }
            });
        }

        private void updateRuntime(int currentTime) {

            if (mRunTime == null) {
                // this view can be null if the user
                // does not want to use it. Don't throw
                // an exception.
                return;
            }

            if (currentTime < 0) {
                throw new IllegalArgumentException("ERROR_PLAYTIME_CURRENT_NEGATIVE");
            }

            StringBuilder playbackStr = new StringBuilder();

            // set the current time
            // its ok to show 00:00 in the UI
            playbackStr.append(String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes((long) currentTime), TimeUnit.MILLISECONDS.toSeconds((long) currentTime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) currentTime))));

            mRunTime.setText(playbackStr);

            // DebugLog.i(currentTime + " / " + totalDuration);
        }

        private void setTotalTime() {

            if (mTotalTime == null) {
                // this view can be null if the user
                // does not want to use it. Don't throw
                // an exception.
                return;
            }

            StringBuilder playbackStr = new StringBuilder();
            long totalDuration = 0;

            // by this point the media player is brought to ready state
            // by the call to init().
            if (mMediaPlayer != null) {
                try {
                    totalDuration = mMediaPlayer.getDuration();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (totalDuration < 0) {
                return;
//                throw new IllegalArgumentException("ERROR_PLAYTIME_TOTAL_NEGATIVE");
            }

            // set total time as the audio is being played
            if (totalDuration != 0) {
                playbackStr.append(String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes((long) totalDuration), TimeUnit.MILLISECONDS.toSeconds((long) totalDuration) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) totalDuration))));
            }

            mTotalTime.setText(playbackStr);
        }

        private void setViewsVisibility() {

            if (mSeekBar != null) {
                mSeekBar.setVisibility(View.VISIBLE);
            }


            if (mRunTime != null) {
                mRunTime.setVisibility(View.VISIBLE);
            }

            if (mTotalTime != null) {
                mTotalTime.setVisibility(View.VISIBLE);
            }

            if (mPlayButton != null) {
                mPlayButton.setVisibility(View.VISIBLE);
            }

            if (mPauseButton != null) {
                mPauseButton.setVisibility(View.VISIBLE);
            }
        }

        private void initPlayer(Context ctx) {

            mMediaPlayer = new MediaPlayer();


            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

            try {
                mMediaPlayer.setDataSource(ctx, mUri);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (SecurityException e) {
                e.printStackTrace();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                mMediaPlayer.prepare();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            mMediaPlayer.setOnCompletionListener(onCompletionListener);
        }

        private void setPlayable() {
            if (mPlayButton != null) {
                mPlayButton.setVisibility(View.VISIBLE);
            }

            if (mPauseButton != null) {
                mPauseButton.setVisibility(View.GONE);
            }
        }

        private ArrayList<MediaPlayer.OnCompletionListener> mCompletionListeners = new ArrayList<MediaPlayer.OnCompletionListener>();

        private void fireCustomCompletionListeners(MediaPlayer mp) {
            for (MediaPlayer.OnCompletionListener listener : mCompletionListeners) {
                listener.onCompletion(mp);
            }
        }

        private MediaPlayer.OnCompletionListener onCompletionListener = new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                // set UI when audio finished playing
                int currentPlayTime = 0;
                mSeekBar.setProgress((int) currentPlayTime);

                updateRuntime(currentPlayTime);
                setPlayable();
                // ensure that our completion listener fires first.
                // This will provide the developer to over-ride our
                // completion listener functionality

                fireCustomCompletionListeners(mp);

                mProgressUpdateHandler.removeCallbacks(mUpdateProgress);
            }
        };


        //        private MediaPlayer.OnCompletionListener mOnCompletion;


        private Runnable mUpdateProgress = new Runnable() {

            public void run() {

                if (mSeekBar == null) {
                    return;
                }

                if (mProgressUpdateHandler != null && mMediaPlayer.isPlaying()) {
                    mSeekBar.setProgress((int) mMediaPlayer.getCurrentPosition());
                    int currentTime = mMediaPlayer.getCurrentPosition();
                    updateRuntime(currentTime);
                    // repeat the process
                    mProgressUpdateHandler.postDelayed(mUpdateProgress, AUDIO_PROGRESS_UPDATE_TIME);
                } else {
                    // DO NOT update UI if the player is paused
                }
            }
        };
    }

    public View createDrawableFromView(View view, String url) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        SimpleDraweeView image = (SimpleDraweeView) view.findViewById(R.id.thumbnail);

//            View view= ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_image_gallery, null);
//            loadImageInto(view,position);

        if (!url.equals("0") && !url.equals("")) {
            if (!url.contains("http"))
                image.setImageURI(Uri.parse("file://" + url));
            else image.setImageURI(Uri.parse(url));
        } else {
            image.setImageResource(android.R.drawable.ic_menu_gallery);
        }


        return view;
    }

    public FragmentManager fragmentManager;
    public android.support.v4.app.FragmentTransaction fragmentTransaction;
    Fragment fragment;

    public void openMedia(String url) {


        fragment = VideoFragment.newInstance(url);


        if (fragmentManager == null) {
            fragmentManager = getSupportFragmentManager();
        }
        fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.setCustomAnimations(R.anim.push_left_in, R.anim.push_right_out,
                R.anim.slid_in, R.anim.slide_out);
        fragmentTransaction.addToBackStack(null);

//        if (url.toLowerCase().contains("youtube"))
//            fragmentTransaction.add(R.id.mediaFragmentContainer, fragment);
//        else
        fragmentTransaction.add(R.id.mediaFragmentContainer
                , fragment);

        fragmentTransaction.commit();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }




    private void share(int position) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, mediaCursor.get(position).getLink());
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));
    }

    private void download(int position) {
        ProgressDialog mProgressDialog;

// instantiate it within the onCreate method
        mProgressDialog = new ProgressDialog(GalleryPagerActivity.this);
        mProgressDialog.setMessage("downloading");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setCancelable(true);

        Log.d("testMeidaType", String.valueOf(mediaCursor.get(position).getMediaType())+ " " + mediaCursor.get(position).getLink());

// execute this when the downloader must be fired
        final DownloadTask downloadTask = new DownloadTask(GalleryPagerActivity.this,mediaCursor.get(position).getMediaType());
        downloadTask.execute(mediaCursor.get(position).getLink());

        mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                downloadTask.cancel(true);
            }
        });
    }

}


