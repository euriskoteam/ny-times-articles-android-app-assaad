package com.apps2you.nytimeapplication.gallery;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.support.design.widget.Snackbar;

import com.apps2you.nytimeapplication.GlobalMethods;
import com.apps2you.nytimeapplication.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadTask extends AsyncTask<String, Integer, String> {

    private Activity context;
    private PowerManager.WakeLock mWakeLock;
    private int type;

    public DownloadTask(Activity context, int type) {
        this.context = context;
        this.type = type;
    }

    @Override
    protected String doInBackground(String... sUrl) {
        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(sUrl[0]);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            // expect HTTP 200 OK, so we don't mistakenly save error report
            // instead of the file`
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return "Server returned HTTP " + connection.getResponseCode()
                        + " " + connection.getResponseMessage();
            }

            // this will be useful to display download percentage
            // might be -1: server did not report the length
            int fileLength = connection.getContentLength();

            // download the file
            input = connection.getInputStream();
            File file;
            output = new FileOutputStream(file = GlobalMethods.createMediaFile(GlobalMethods.folderPath, getExtension()));


            context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)));

//            Log.d("testpaTHHHH", String.valueOf(GlobalMethods.createMediaFile(GlobalMethods.folderPath,".png")));

            byte data[] = new byte[4096];
            long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
                // allow canceling with back button
                if (isCancelled()) {
                    input.close();
                    return null;
                }
                total += count;
                // publishing the progress....
                if (fileLength > 0) // only if total length is known
                    publishProgress((int) (total * 100 / fileLength));
                output.write(data, 0, count);
            }

//            SnackS.snackAlert(context,context.getString(R.string.download_complete));

        } catch (Exception e) {
            return e.toString();
        } finally {
            try {
                if (output != null)
                    output.close();
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
            }

            if (connection != null)
                connection.disconnect();
        }
        return null;
    }


    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);


    }

    String getExtension() {
        switch (type) {
            case GalleryPagerActivity.TYPE_AUDIO:
                return ".mp3";
            case GalleryPagerActivity.TYPE_VIDEO:
                return ".mp4";
            case GalleryPagerActivity.TYPE_IMAGE:
                return ".png";
            default:
                return ".png";
        }
    }
}