//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.apps2you.nytimeapplication.gallery;

import android.content.Context;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;

import com.apps2you.nytimeapplication.R;

public class VideoFragment extends Fragment {
    private static final String ARG_VIDEO_URL = "video_url";
    public MediaController mediaController;
    private VideoView videoView;
    private ImageView iv_back;
    private ProgressBar progressbar;
    private String video_url;
    private VideoFragment.OnFragmentInteractionListener mListener;

    public VideoFragment() {
    }

    public static VideoFragment newInstance(String video_url) {
        VideoFragment fragment = new VideoFragment();
        Bundle args = new Bundle();
        args.putString("video_url", video_url);
        fragment.setArguments(args);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (this.getArguments() != null) {
            this.video_url = this.getArguments().getString("video_url");
        }

    }

    public void onResume() {
        super.onResume();
        this.mediaController = new MediaController(this.getActivity());
        this.mediaController.setAnchorView(this.videoView);
        this.videoView.setMediaController(this.mediaController);
        this.videoView.setVideoURI(Uri.parse(this.video_url));
        this.videoView.requestFocus();
        this.videoView.start();
        OnPreparedListener mediaPlayerOnPreparedListener = new OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                VideoFragment.this.progressbar.setVisibility(View.GONE);
            }
        };
        this.videoView.setOnPreparedListener(mediaPlayerOnPreparedListener);
        this.videoView.setOnErrorListener(new OnErrorListener() {
            public boolean onError(MediaPlayer mp, int what, int extra) {
                return false;
            }
        });
        this.videoView.setOnCompletionListener(new OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                VideoFragment.this.onButtonPressed(Uri.parse(VideoFragment.this.video_url));
            }
        });
        this.iv_back.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                VideoFragment.this.onButtonPressed(Uri.parse(VideoFragment.this.video_url));
            }
        });
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.video_fragment, container, false);
        this.iv_back = (ImageView)view.findViewById(R.id.video_fragment_back);
        this.videoView = (VideoView)view.findViewById(R.id.video_view);
        FrameLayout parentContainer = (FrameLayout)view.findViewById(R.id.frame_video_container);
        parentContainer.setBackgroundColor(Color.BLACK);
        this.progressbar = (ProgressBar)view.findViewById(R.id.video_fragment_progress);
        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (this.mListener != null) {
            this.mListener.onFragmentInteraction(uri);
        }

    }

    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof VideoFragment.OnFragmentInteractionListener) {
            this.mListener = (VideoFragment.OnFragmentInteractionListener)context;
        }

    }

    public void onDetach() {
        super.onDetach();
        this.mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri var1);
    }
}
