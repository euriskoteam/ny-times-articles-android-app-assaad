
package com.apps2you.nytimeapplication.mainPage.serverModels;

import com.apps2you.nytimeapplication.gallery.GalleryObjectInterface;
import com.apps2you.nytimeapplication.gallery.GalleryPagerActivity;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Generated;
import org.parceler.Parcel;

@Generated("org.jsonschema2pojo")
@Parcel(Parcel.Serialization.BEAN)
public class MediaMetadatum implements GalleryObjectInterface {

    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("format")
    @Expose
    private String format;
    @SerializedName("height")
    @Expose
    private Integer height;
    @SerializedName("width")
    @Expose
    private Integer width;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }


    @Override
    public int getMediaType() {
        return GalleryPagerActivity.TYPE_IMAGE;
    }

    @Override
    public String getLink() {
        return getUrl();
    }

    @Override
    public String getThumb() {
        return "";
    }

    @Override
    public String getName() {
        return getFormat();
    }
}
