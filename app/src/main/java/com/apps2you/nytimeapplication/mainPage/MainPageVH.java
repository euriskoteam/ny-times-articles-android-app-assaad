package com.apps2you.nytimeapplication.mainPage;

import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps2u.happiestrecyclerview.ViewHolder;
import com.apps2you.nytimeapplication.R;
import com.facebook.drawee.view.SimpleDraweeView;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by ama on 2/23/2016.
 */

public class MainPageVH extends ViewHolder {


    @BindView(R.id.image)
    SimpleDraweeView image;
    @BindView(R.id.title_tv)
    TextView titleTv;
    @BindView(R.id.description_tv)
    TextView descriptionTv;
    @BindView(R.id.date_tv)
    TextView dateTv;
    @BindView(R.id.arrow_iv)
    ImageView arrowIv;

    public MainPageVH(final View parent) {
        super(parent);
        ButterKnife.bind(this, parent);
    }


    public void setData(NYTimeInterface nyTimeInterface) {
        image.setImageURI(Uri.parse(nyTimeInterface.getNYImage()));
        titleTv.setText(nyTimeInterface.getNYTitle());
        descriptionTv.setText(nyTimeInterface.getNYDescription());
        dateTv.setText(nyTimeInterface.getNYDate());
    }


}