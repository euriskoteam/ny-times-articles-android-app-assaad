package com.apps2you.nytimeapplication.mainPage;

import android.content.Context;

import java.io.Serializable;

public interface NYTimeInterface   {
    String getNYDate();
    String getNYTitle();
    String getNYImage();
    String getNYDescription();
}
