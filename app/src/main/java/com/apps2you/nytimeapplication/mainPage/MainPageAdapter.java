package com.apps2you.nytimeapplication.mainPage;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.apps2u.happiestrecyclerview.RecyclerView;
import com.apps2u.happiestrecyclerview.RecyclerViewAdapter;
import com.apps2u.happiestrecyclerview.ViewHolder;
import com.apps2you.nytimeapplication.R;

/**
 * Created by appsassaad on 5/18/2018.
 */

public class MainPageAdapter extends RecyclerViewAdapter<MainPageVH,NYTimeMockObject> {



    public MainPageAdapter(Activity context, RecyclerView recyclerView) {
        super(context, recyclerView);
    }

    @Override
    public boolean attachAlwaysLastHeader() {
        return false;
    }

    @Override
    public void onBindViewHolders(MainPageVH viewHolder, int position) {
        viewHolder.setData(getData().get(position));
    }

    @Override
    public ViewHolder onCreateViewHolders(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        return new MainPageVH(layoutInflater.inflate(R.layout.ny_time_row, parent, false));
    }

    @Override
    public int getHeaderLayout() {
        return 0;
    }

    @Override
    public int getFooterLayout() {
        return 0;
    }

    @Override
    public boolean isStickyHeader() {
        return false;
    }

    @Override
    public int getItemType(int i) {
        return 1;
    }

    @Override
    public int getOrientation() {
        return LinearLayout.VERTICAL;
    }

    @Override
    public String getSectionCondition(int position) {
        return "";
    }

    @Override
    public boolean isSection() {
        return false;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return 0;
    }



}
