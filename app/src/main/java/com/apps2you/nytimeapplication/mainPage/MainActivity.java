package com.apps2you.nytimeapplication.mainPage;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;

import com.apps2u.happiestrecyclerview.OnItemClickListener;
import com.apps2u.happiestrecyclerview.RecyclerView;
import com.apps2u.happiestrecyclerview.SwipeListener;
import com.apps2you.nytimeapplication.BaseActivity;
import com.apps2you.nytimeapplication.R;
import com.apps2you.nytimeapplication.connectivity.APIService;
import com.apps2you.nytimeapplication.connectivity.ApisHelper;
import com.apps2you.nytimeapplication.connectivity.BaseResponse;
import com.apps2you.nytimeapplication.connectivity.EmptyObject;
import com.apps2you.nytimeapplication.mainPage.serverModels.Result;
import com.google.gson.reflect.TypeToken;

import org.parceler.Parcels;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements SwipeListener {


    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    MainPageAdapter mainPageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initRecyclerView();
        getDataFromServer();
    }



    void getDataFromServer(){
        makeHit(ApisHelper.BASE_URL + "mostviewed/all-sections/1.json?api-key=Rk6GfVjL9XA3A5ipo7bjr2fNh80CpeA5",
                Parcels.wrap(new EmptyObject()),
                ApisHelper.action_GET_NY_TIME,
                APIService.GET_TYPE,
                ApisHelper.getApiHeaders(),
                new ArrayList<String>(),
                new ArrayList<String>()
        );
    }

    void initRecyclerView() {
        recyclerView.setAdapter(mainPageAdapter = new MainPageAdapter(this,recyclerView));
        recyclerView.setSwipeListener(this);
        mainPageAdapter.setOnItemClickedListener(this, android.R.color.darker_gray, new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int i) {
                NYDetailsActivity.Launch(MainActivity.this,  mainPageAdapter.getData().get(i));
              ;
            }

            @Override
            public void onItemLongClick(View view, int i) {

            }
        });
    }


    @Override
    public void onHttpResponse(String action, String objectReceived, Object objectSent) {
        dismissLoader();
            switch (action){
                case ApisHelper.action_GET_NY_TIME:

                   ArrayList<NYTimeMockObject> nyTimeInterfaces = getObjectFromResponse(objectReceived, new TypeToken<BaseResponse<ArrayList<NYTimeMockObject>>>() {
                    });
                    mainPageAdapter.setData(nyTimeInterfaces);
                    break;
            }
    }

    @Override
    public void onHttpRequestStarted(String action, Object objectSent) {
        switch (action){
            case ApisHelper.action_GET_NY_TIME:
                showLoader(getString(R.string.loading));
                break;
        }
    }

    @Override
    public void onConnectionError(String action, Object objectSent) {
        dismissLoader();
        switch (action){
            case ApisHelper.action_GET_NY_TIME:
                Snackbar.make(recyclerView, getString(R.string.connection_error), 2000).show();
                break;
        }
    }

    @Override
    public void onResponseServerError(String action, String errorCode, String objectReceived, Object objectSent) {
        dismissLoader();
        switch (action){
            case ApisHelper.action_GET_NY_TIME:
                // TODO: 2/20/2019 any action when server return error
                break;
        }
    }

    @Override
    public void onSwipe() {
        getDataFromServer();
    }

    @Override
    public void onSwipeConnectionError() {

    }

    @Override
    public void loadMore(int i) {

    }
}
