package com.apps2you.nytimeapplication.mainPage;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.apps2you.nytimeapplication.BaseActivity;
import com.apps2you.nytimeapplication.R;
import com.apps2you.nytimeapplication.gallery.GalleryObjectInterface;
import com.apps2you.nytimeapplication.gallery.GalleryPagerActivity;
import com.apps2you.nytimeapplication.mainPage.serverModels.MediaMetadatum;
import com.apps2you.nytimeapplication.mainPage.serverModels.Medium;

import org.parceler.Parcels;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NYDetailsActivity extends BaseActivity {

    private static final String ARG_NY_OBJECT = "ARG_NY_OBJECT";

    public static void Launch(Context context, NYTimeMockObject nyTimeMockObject) {
        Intent intent = new Intent(context, NYDetailsActivity.class);
        intent.putExtra(ARG_NY_OBJECT, Parcels.wrap(nyTimeMockObject));
        context.startActivity(intent);
    }

    NYTimeMockObject getNyObject() {
        return Parcels.unwrap(getIntent().getParcelableExtra(ARG_NY_OBJECT));
    }

    @BindView(R.id.title_tv)
    TextView titleTv;
    @BindView(R.id.section_tv)
    TextView sectionTv;
    @BindView(R.id.byline_tv)
    TextView bylineTv;
    @BindView(R.id.type_tv)
    TextView typeTv;
    @BindView(R.id.source_tv)
    TextView sourceTv;
    @BindView(R.id.views_tv)
    TextView viewsTv;
    @BindView(R.id.published_date_tv)
    TextView publishedDateTv;
    @BindView(R.id.show_media_btn)
    Button showMediaBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nydetails);
        ButterKnife.bind(this);
        initData();
    }


    void initData() {
        NYTimeMockObject nyTimeMockObject = getNyObject();
        titleTv.setText(nyTimeMockObject.getTitle());
        sectionTv.setText(nyTimeMockObject.getSection());
        bylineTv.setText(nyTimeMockObject.getByline());
        typeTv.setText(nyTimeMockObject.getType());
        sourceTv.setText(nyTimeMockObject.getSource());
        viewsTv.setText(nyTimeMockObject.getViews() + "");
        publishedDateTv.setText(nyTimeMockObject.getPublishedDate());
    }

    @Override
    public void onHttpResponse(String action, String objectReceived, Object objectSent) {

    }

    @Override
    public void onHttpRequestStarted(String action, Object objectSent) {

    }

    @Override
    public void onConnectionError(String action, Object objectSent) {

    }

    @Override
    public void onResponseServerError(String action, String errorCode, String objectReceived, Object objectSent) {

    }

    @OnClick(R.id.show_media_btn)
    public void onViewClicked() {
        ArrayList<GalleryObjectInterface> arrayList = new ArrayList<>();
        for (Medium medium : getNyObject().getMedia()) {
            for (MediaMetadatum mediaMetadatum : medium.getMediaMetadata()) {
                arrayList.add(mediaMetadatum);
            }

        }

        GalleryPagerActivity.Launch(this, arrayList, 0);
    }
}
