package com.apps2you.nytimeapplication;

import android.os.Environment;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GlobalMethods {

    public static final String TEMP_MEDIA_FILE_PATH = ApplicationContext.getContext().getString(R.string.app_name) + "/";
    public static final String folderPath = TEMP_MEDIA_FILE_PATH + "files/pictures/";

    public static File createMediaFile(String filePath, String ext) throws IOException {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = timeStamp;

        File storageDir = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.FROYO) {
            storageDir = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS), filePath);
        }

        if (!storageDir.exists())
            storageDir.mkdirs();

        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ext,         /* suffix */
                storageDir      /* directory */
        );

        if (!image.exists())
            image.mkdirs();


        return image;
    }

}
