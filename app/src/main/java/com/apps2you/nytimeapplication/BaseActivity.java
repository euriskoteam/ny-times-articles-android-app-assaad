package com.apps2you.nytimeapplication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;

import com.apps2you.nytimeapplication.connectivity.APIService;
import com.apps2you.nytimeapplication.connectivity.ApiResultReceiver;
import com.apps2you.nytimeapplication.connectivity.BaseResponse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;


public abstract class BaseActivity extends AppCompatActivity implements ApiResultReceiver.Receiver {

    public ApiResultReceiver apiResultReceiver;
    public Handler uiHandler;
    ProgressDialog progress;
    private boolean isForeground = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        uiHandler = new Handler();
        apiResultReceiver = new ApiResultReceiver(uiHandler);
        apiResultReceiver.setReceiver(this);

    }



    public void showLoader(String message, boolean cancelable) {
        if (BaseActivity.this != null)
            if (!isFinishing()) {
                if (progress != null)
                    if (progress.isShowing())
                        progress.dismiss();
                progress = null;
                progress = new ProgressDialog(BaseActivity.this);
                progress.setCanceledOnTouchOutside(cancelable);
                progress.setCancelable(cancelable);
                progress.setMessage(message);
                progress.show();
            }
    }

    public void showLoader(String message) {
        if (BaseActivity.this != null)
            if (!isFinishing()) {
                if (progress != null)
                    if (progress.isShowing())
                        progress.dismiss();
                progress = null;
                progress = new ProgressDialog(BaseActivity.this);
                progress.setCanceledOnTouchOutside(false);
                progress.setMessage(message);
                progress.show();
            }
    }

    public boolean isShowingLoader() {
        if (!isFinishing())
            if (progress != null)
                if (progress.isShowing())
                    return true;
        return false;
    }

    public void dismissLoader() {
        if (progress != null && !isFinishing())
            progress.dismiss();
    }



    public  <T> T getObjectFromResponse(String r, TypeToken typeToken) {
        Gson gson = new Gson();
        BaseResponse<T> baseResponse = gson.fromJson(r, typeToken.getType());
        return  baseResponse.getData();
    }

    public String getMessageFromResponse(String r) {
        Gson gson = new Gson();
        BaseResponse baseResponse = gson.fromJson(r,new TypeToken<BaseResponse<?>>(){}.getType());
        return  baseResponse.getMessage();
    }
    public String getErrorFromResponse(String r) {
        Gson gson = new Gson();
        BaseResponse baseResponse = gson.fromJson(r,new TypeToken<BaseResponse<?>>(){}.getType());
        return  baseResponse.getStatus();
    }

    public void makeHit(String URL, Parcelable objectToSent, String actionName, int hitType, HashMap<String, String> headers, ArrayList<String> filePaths, ArrayList<String> fileKeys) {
        Intent intent = new Intent(this, APIService.class);
        intent.putExtra(APIService.HEADERS, headers);
        intent.putExtra(APIService.URL, URL);
        intent.setAction(actionName);
        intent.putExtra("receiver", apiResultReceiver);
        intent.putExtra(APIService.DATA_SENT, objectToSent);
        intent.putExtra(APIService.HIT_TYPE, hitType);
        intent.putStringArrayListExtra(APIService.linkToUpload, filePaths == null ? new ArrayList<String>() : filePaths);
        intent.putStringArrayListExtra(APIService.linkToUploadKey, fileKeys == null ? new ArrayList<String>() : fileKeys);
        startService(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        isForeground = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        isForeground = false;
    }

    public boolean isForeground() {
        return this.isForeground;
    }

}
