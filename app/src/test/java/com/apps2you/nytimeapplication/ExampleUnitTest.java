package com.apps2you.nytimeapplication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Parcelable;

import com.apps2you.nytimeapplication.connectivity.APIService;
import com.apps2you.nytimeapplication.connectivity.ApiResultReceiver;
import com.apps2you.nytimeapplication.connectivity.ApisHelper;
import com.apps2you.nytimeapplication.connectivity.BaseResponse;
import com.apps2you.nytimeapplication.connectivity.EmptyObject;
import com.apps2you.nytimeapplication.mainPage.MainActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(MockitoJUnitRunner.class)
public class ExampleUnitTest{

    @Test
    public void addition_isCorrect() {
        assertEquals("OK", "OK");
    }


}