package com.apps2you.nytimeapplication.mainPage;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class NYTimeMockObjectTest {

    @Mock
    NYTimeMockObject nyTimeMockObject;


    @Test
    public void getNYDate() {
        when(nyTimeMockObject.getNYDate()).thenReturn("2019-02-15");

        String date = nyTimeMockObject.getNYDate();
        long dateLong = getTimeInMillisecondsFromDate(date);
        assertEquals(dateLong,1547503320000L);
    }

    @Test
    public void getNYDateIncoorectInput() {
        when(nyTimeMockObject.getNYDate()).thenReturn("2019/02/15");

        String date = nyTimeMockObject.getNYDate();
        long dateLong = getTimeInMillisecondsFromDate(date);
        assertNotEquals(dateLong,1547503320000L);
    }

    public  Long getTimeInMillisecondsFromDate(String givenDateString) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
        long timeInMilliseconds = 0L;
        try {
            Date mDate = sdf.parse(givenDateString);
            timeInMilliseconds = mDate.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timeInMilliseconds;
    }

    @Test
    public void getNYTitle() {
    }

    @Test
    public void getNYImage() {
    }

    @Test
    public void getNYDescription() {
    }


}