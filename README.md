This Project contains 3 packages

1. mainPage package     :  contains a list of data retrieved from server 
2. connectivity package :  handle the Restfull 
3. gallery package      :  for showing images, videos and audio in a custom view pager

Dependencies usesd:
 - HappiestRecyclerView :	Custom built recyclerview helper library
 - Fresco
 - Retrofit
 - Json Parcelable
 - Butterknife
 - Gson